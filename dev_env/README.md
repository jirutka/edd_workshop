# gitlab CI/CD environment

## get docker image

Create docker image

```bash
docker build -t registry.gitlab.com/jurafejfar/edd_workshop:pg12 .
docker login registry.gitlab.com
docker push registry.gitlab.com/jurafejfar/edd_workshop:pg12
```

or pull directly from repository

```bash
docker login registry.gitlab.com
docker pull registry.gitlab.com/jurafejfar/edd_workshop:pg12
```

## run image locally
```bash
docker image ls
docker run -i -t -v `pwd`:/home/vagrant/work_dir -p 5432:5432 --name dejvice_production --hostname dejvice_production registry.gitlab.com/jurafejfar/edd_workshop:pg12 /bin/bash
docker container ls -a
docker container rm -v edd_workshop
```

for GUI apps with VcXsrv X Server
```
docker run -i -t -v `pwd`:/home/vagrant/work_dir -p 5432:5432 --name dejvice_production --hostname dejvice_production -e DISPLAY=192.168.77.41:0.0 registry.gitlab.com/jurafejfar/edd_workshop:pg12 /bin/bash
```

## set-up local environment and run CI/CD jobs locally
```
sudo dpkg-reconfigure tzdata
# git clone <dotfiles>
# sudo apt install ssh && set up or copy ssh keys && chmod 700 ~/.ssh/id_rsa
sudo mkdir -p /builds/edd_workshop
sudo chown -R vagrant /builds
cd /builds/edd_workshop
git clone git@gitlab.com:jurafejfar/edd_workshop.git
git clone git@gitlab.com:jurafejfar/edd_workshop.wiki.git
sudo -u postgres psql -c "create user vagrant with superuser;"
# sudo make install
# make installcheck
```

## run CI/CD jobs locally (gitlab-runner)

https://docs.gitlab.com/runner/install/linux-repository.html

```bash
gitlab-runner exec docker test1
gitlab-runner exec docker --docker-volumes `pwd`/build-output:/builds test1
```

## remove all docker images and containers
```bash
docker rm `docker ps -aq`
docker system prune -a
```